#/bin/sh

shared() {
    mkdir -p /var/log/torthmcu
    chown minecraft:minecraft /var/log/torthmcu
    mkdir -p /var/run/torthmcu
    chown minecraft:minecraft /var/run/torthmcu

    /usr/bin/systemctl daemon-reload
    /usr/bin/systemctl enable torthmcu-backup-hourly.timer
    /usr/bin/systemctl start torthmcu-backup-hourly.timer
    /usr/bin/systemctl enable torthmcu-backup-daily.timer
    /usr/bin/systemctl start torthmcu-backup-daily.timer
    /usr/bin/systemctl enable torthmcu-restart-minecraft-daily.timer
    /usr/bin/systemctl start torthmcu-restart-minecraft-daily.timer
}

install() {
    echo "post-install part"
    shared
}

upgrade() {
    echo "post-install-upgrade part"
    shared
}

case ${1} in
    1) # Install
        install
        ;;
    2) # Upgrade
        upgrade
        ;;
esac
