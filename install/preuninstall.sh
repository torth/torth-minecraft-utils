#/bin/sh

uninstall() {
    echo "pre-uninstall part"
    rm -rf /var/log/torthmcu
    rm -rf /var/run/torthmcu

    /usr/bin/systemctl disable torthmcu-backup-hourly.timer
    /usr/bin/systemctl stop torthmcu-backup-hourly.timer
    /usr/bin/systemctl disable torthmcu-backup-daily.timer
    /usr/bin/systemctl stop torthmcu-backup-daily.timer
    /usr/bin/systemctl disable torthmcu-restart-minecraft-daily.timer
    /usr/bin/systemctl stop torthmcu-restart-minecraft-daily.timer
}

upgrade() {
    echo "pre-uninstall-upgrade part"
}

case ${1} in
    0) # Uninstall
        uninstall
        ;;
    1) # Upgrade
        upgrade
        ;;
esac
