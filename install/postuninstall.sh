#/bin/sh

shared() {
    /usr/bin/systemctl daemon-reload
}

uninstall() {
    echo "post-uninstall part"
    shared
}

upgrade() {
    echo "post-uninstall-upgrade part"
    shared
}

case ${1} in
    0) # Uninstall
        uninstall
        ;;
    1) # Upgrade
        upgrade
        ;;
esac
