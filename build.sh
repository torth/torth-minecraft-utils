#!/usr/bin/env bash

BASE_NAME=$(basename "$0")
BASE_DIR="$(dirname "$(readlink -f -- "$0")")"
PACKAGE_DIR="build/package"

MODE="--binary-only"
KEEPTEMP="--no-keep-temp"
VERBOSE=""
DRY_RUN=""
STATIC_ARGS=(
    "--no-autoreq"
    "--force-arch" "x86_64"
    "--dist-dir" "${PACKAGE_DIR}"
    "--pre-install" "install/preinstall.sh"
    "--pre-uninstall" "install/preuninstall.sh"
    "--post-install" "install/postinstall.sh"
    "--post-uninstall" "install/postuninstall.sh"
)

usage() {
    echo "Usage: ${BASE_DIR}/${BASE_NAME} [--binary-only] [--spec-only]"
    echo "  --binary-only        only generate binary RPM [default]"
    echo "  --spec-only          only regenerate spec file"
    echo "  --no-keep-temp       clean up RPM build directory [default]"
    echo "  --keep-temp          don't clean up RPM build directory"
    echo "  --dry-run (-n)       don't generate anything, print what would be done"
    exit 1
}

get_git_revision() {
    # Get last commit date
    git show -s --format=%ai | sed -n 's/\([^ ]\+\) \([^:]\+\):\([^:]\+\).*/\1-\2-\3/p' | tr -d "-" | sort -n | tail -1
}

# Parse arguments
while (( $# )); do
    case ${1} in
        --binary-only|--spec-only)
            MODE="${1}"
            ;;
        --keep-temp|--no-keep-temp)
            KEEPTEMP="${1}"
            ;;
        -h|--help)
            usage
            ;;
        -v|--verbose)
            VERBOSE="-v"
            ;;
        --version)
            export VERSION="${2}"
            shift
            ;;
        -n|--dry-run)
            DRY_RUN="-n"
            ;;
        *)
            echo "Invalid argument: ${1}"
            echo
            usage
            ;;
    esac
    shift
done

# Remove old packages
if [[ -d "${BASE_DIR}/${PACKAGE_DIR}" ]]; then
    rm -rf "${BASE_DIR:?}/${PACKAGE_DIR}"
fi
mkdir -p "${BASE_DIR}/${PACKAGE_DIR}"

python3 setup.py bdist_rpm ${DRY_RUN} ${VERBOSE} "${MODE}" "${KEEPTEMP}" "${STATIC_ARGS[@]}" --release "$(get_git_revision)"
