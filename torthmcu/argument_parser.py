import argparse

import torthmcu.constants as C


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self, namespaces: list = None, **kwargs):
        prog = C.APP_NAME
        if "prog" in kwargs:
            prog = kwargs.pop("prog")
        super().__init__(
            prog=prog,
            description=C.APP_DESC,
            formatter_class=argparse.RawDescriptionHelpFormatter,
            **kwargs
        )
        if namespaces:
            self.generate_subparsers(namespaces)

    def generate_subparsers(self, namespaces: list):
        subparsers = self.add_subparsers()
        for namespace in namespaces:
            namespace_parser = subparsers.add_parser(
                namespace.name, help=namespace.description
            )
            namespace_subparsers = namespace_parser.add_subparsers(help="actions")

            for action in namespace.actions:
                action_parser = namespace_subparsers.add_parser(
                    action.name, help=action.description
                )
                action_parser.set_defaults(func=action.func)
                for arg, kwargs in action.arguments:
                    action_parser.add_argument(arg, **kwargs)

    def __repr__(self) -> str:
        return "ArgumentParser"
