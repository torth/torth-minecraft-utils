from abc import ABC
from abc import abstractmethod

from typing import List
from dataclasses import dataclass, field

from torthmcu.action import Action


@dataclass
class Namespace(ABC):
    actions: List[Action] = field(default_factory=list)

    def add_action(self, action: Action):
        if not isinstance(action, Action):
            raise TypeError()
        self.actions.append(action)

    @property
    @abstractmethod
    def name(self) -> str:
        pass

    @property
    @abstractmethod
    def description(self) -> str:
        pass
