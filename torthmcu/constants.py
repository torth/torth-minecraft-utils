from pathlib import Path

RUN_AS_USER = "minecraft"
RUN_AS_GRP = "minecraft"
APP_NAME = "TOrth's Minecraft Utils"
APP_DESC = "Utilities for Minecraft Server by TOrth"
APP_CONF_PATH = Path("/etc/sysconfig/torthmcu.json")
APP_LOG_PATH = Path("/var/log/torthmcu/torthmcu.log")
SERVICE_NAME = "minecraft"
BACKUP_CONF = "backups.json"
