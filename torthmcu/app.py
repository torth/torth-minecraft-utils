import sys
import logging

from typing import List
from logging import handlers, Logger
from dataclasses import dataclass, field

import torthmcu.constants as C

from torthmcu.config import AppConfig
from torthmcu.action import ActionFailedError
from torthmcu.namespace import Namespace
from torthmcu.argument_parser import ArgumentParser


@dataclass
class App:
    return_code: int = field(default=0, init=False)
    namespaces: List[Namespace] = field(default_factory=list, init=False)
    logger: Logger = field(init=False)

    def __post_init__(self):
        self.logger = self.init_logging(AppConfig().get("APP_LOG_LEVEL", 20))

    def add_namespace(self, namespace: Namespace):
        if not isinstance(namespace, Namespace):
            raise TypeError
        self.namespaces.append(namespace)

    @staticmethod
    def init_logging(log_level: int) -> logging.Logger:
        logger = logging.getLogger(C.APP_NAME)
        logger.setLevel(log_level)

        file_formatter = logging.Formatter(
            "%(asctime)s - %(name)s[%(process)d]: %(levelname)s: %(message)s"
        )
        file_handler = handlers.TimedRotatingFileHandler(
            filename=C.APP_LOG_PATH, when="midnight", backupCount=7
        )
        file_handler.setFormatter(file_formatter)
        logger.addHandler(file_handler)

        stream_formatter = logging.Formatter("%(levelname)s - %(message)s")
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(stream_formatter)
        logger.addHandler(stream_handler)

        return logger

    def run(self):
        parser = ArgumentParser(namespaces=self.namespaces)
        args = parser.parse_args(sys.argv[1:])
        if not hasattr(args, "func"):
            parser.print_help(sys.stderr)
            self.return_code = 1
            return
        self.logger.debug("Application started with following arguments: %s", args)
        try:
            args.func(args)
        except ActionFailedError as error:
            self.return_code = 1
            self.logger.critical(f"Action failed: {str(error)}.")
        except Exception as error:
            self.return_code = 1
            self.logger.critical(f"Unexpected error occured: {str(error)}.")
            raise RuntimeError from error
