from torthmcu.action import Action as BaseAction
from torthmcu.minecraft import MinecraftService


class ActionStop(BaseAction):
    name = "stop"
    description = "Stop the Minecraft Service"

    def __init__(self):
        super().__init__()

    @staticmethod
    def func(_):
        service = MinecraftService()
        service.stop()
