from torthmcu.action import Action as BaseAction
from torthmcu.minecraft import MinecraftService
from torthmcu.service.regular_restart_prevention import RegularRestartPrevention


class ActionRestart(BaseAction):
    name = "restart"
    description = "Restart the Minecraft Service"

    def __init__(self):
        super().__init__()
        self.add_argument(
            "--regular",
            action="store_true",
            help="Specifies if the restart is regular",
        )

    @staticmethod
    def func(args):
        if args.regular:
            rrp = RegularRestartPrevention()
            if rrp.exists:
                rrp.remove()
                return
        service = MinecraftService()
        service.restart()
