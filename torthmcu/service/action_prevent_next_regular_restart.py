from torthmcu.action import Action as BaseAction
from torthmcu.service.regular_restart_prevention import RegularRestartPrevention


class ActionPreventNextRegularRestart(BaseAction):
    name = "prevent-next-regular-restart"
    description = "Prevents Next Regular Restart of the Minecraft Service"

    def __init__(self):
        super().__init__()

    @staticmethod
    def func(_):
        rrp = RegularRestartPrevention()
        rrp.create()
