from pathlib import Path


class RegularRestartPrevention:
    filepath: Path = Path("/var/run/torthmcu/minecraft-regular-restart-prevention")

    @property
    def exists(self):
        return self.filepath.exists()

    def create(self):
        if not self.exists:
            self.filepath.touch()

    def remove(self):
        if self.exists:
            self.filepath.unlink()
