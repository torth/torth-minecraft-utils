from torthmcu.action import Action as BaseAction
from torthmcu.minecraft import MinecraftService


class ActionStatus(BaseAction):
    name = "status"
    description = "Status the Minecraft Service"

    def __init__(self):
        super().__init__()

    @staticmethod
    def func(_):
        service = MinecraftService()
        print(service.status())
