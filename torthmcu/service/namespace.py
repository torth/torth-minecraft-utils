from torthmcu.namespace import Namespace as BaseNamespace

from torthmcu.service.action_start import ActionStart
from torthmcu.service.action_stop import ActionStop
from torthmcu.service.action_status import ActionStatus
from torthmcu.service.action_restart import ActionRestart
from torthmcu.service.action_prevent_next_regular_restart import (
    ActionPreventNextRegularRestart,
)


class Namespace(BaseNamespace):
    name = "service"
    description = "Handles Service Actions."

    def __init__(self):
        super().__init__()
        self.add_action(ActionStart())
        self.add_action(ActionStop())
        self.add_action(ActionStatus())
        self.add_action(ActionRestart())
        self.add_action(ActionPreventNextRegularRestart())
