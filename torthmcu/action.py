from abc import ABC
from abc import abstractmethod

from typing import List, Tuple
from dataclasses import dataclass, field


class ActionFailedError(Exception):
    pass


@dataclass
class Action(ABC):
    arguments: List[Tuple[str, dict]] = field(default_factory=list, init=False)

    def add_argument(self, name: str, **kwargs):
        self.arguments.append((name, kwargs))

    @property
    @abstractmethod
    def name(self) -> str:
        pass

    @property
    @abstractmethod
    def description(self) -> str:
        pass

    @staticmethod
    @abstractmethod
    def func(args) -> bool:
        pass
