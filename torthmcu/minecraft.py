from enum import Enum
from subprocess import Popen, PIPE


class ServiceAction(Enum):
    START = "start"
    STOP = "stop"
    STATUS = "status"
    RESTART = "restart"


class MinecraftServiceError(Exception):
    pass


class MinecraftService:
    def _manage_service(self, action: str):
        cmd = ["sudo", "/usr/bin/systemctl", action, "minecraft"]
        proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = proc.communicate()
        if proc.returncode != 0:
            raise MinecraftServiceError(
                f"Unable to {action} the minecraft service, error: {stderr!r}."
            )
        return stdout.decode("UTF-8")

    def start(self):
        self._manage_service(ServiceAction.START.value)

    def stop(self):
        self._manage_service(ServiceAction.STOP.value)

    def status(self) -> str:
        return self._manage_service(ServiceAction.STATUS.value)

    def restart(self):
        self._manage_service(ServiceAction.RESTART.value)
