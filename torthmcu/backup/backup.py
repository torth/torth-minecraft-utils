import os
import shutil
import tarfile
import datetime
from pathlib import Path
from dataclasses import dataclass, field
from typing import ClassVar, Tuple
from enum import Enum

from torthmcu.config import AppConfig


class BackupType(Enum):
    HOURLY = "hourly"
    DAILY = "daily"
    CUSTOM = "custom"


BACKUP_TYPES = tuple(t.value for t in BackupType)


@dataclass
class Backup:
    type: str
    path: Path = None
    note: str = field(default_factory=str)
    date: datetime.datetime = field(default_factory=datetime.datetime.now)

    def __post_init__(self):
        self.backup_dir = Path(AppConfig().get("MINECRAFT_BACKUP_FOLDER"))
        self.server_dir = Path(AppConfig().get("MINECRAFT_SERVER_FOLDER"))
        if self.type not in BACKUP_TYPES:
            raise ValueError(f"Unknown backup type: {self.type}.")
        if not self.path:
            self.path = self.generate_path()
        self.path = self.path.resolve()

    def __repr__(self) -> str:
        return f"{str(self.date)} ({self.timestamp}) - Type: {self.type} - Note: {self.note}"

    @classmethod
    def from_data(cls, data: dict):
        date = data.get("date")
        if date:
            date = datetime.datetime.fromtimestamp(int(date))
        return cls(
            type=data.get("type", ""),
            path=Path(data.get("path", "")),
            note=data.get("note", ""),
            date=date,
        )

    @property
    def timestamp(self) -> int:
        return int(self.date.replace(microsecond=0).timestamp())

    @property
    def data(self) -> dict:
        return {
            "type": self.type,
            "path": str(self.path),
            "note": self.note,
            "date": self.timestamp,
        }

    def generate_path(self) -> Path:
        return Path.joinpath(
            self.backup_dir,
            f'server-{self.type}-{self.date.strftime("%Y-%m-%d-%H-%M")}.tar.gz',
        )

    def perform(self):
        with tarfile.open(self.path, "w:gz") as tar:
            tar.add(self.server_dir, arcname=os.path.basename(self.server_dir))

    def remove(self):
        self.path.unlink(missing_ok=True)

    def load(self):
        try:
            shutil.rmtree(self.server_dir)
        except FileNotFoundError:
            pass
        with tarfile.open(self.path, "r:gz") as tar:
            tar.extractall(path=self.server_dir.parent)
