import datetime

from torthmcu.action import Action as BaseAction
from torthmcu.action import ActionFailedError

from torthmcu.backup.backup import BACKUP_TYPES
from torthmcu.backup.backup_collection import BackupCollection
from torthmcu.backup.backup_collection import UnknownBackupError


class ActionRemove(BaseAction):
    name = "remove"
    description = "Remove a backup"

    def __init__(self):
        super().__init__()
        self.add_argument(
            "type",
            choices=BACKUP_TYPES,
            help="Specifies a type of the backup",
        )
        self.add_argument(
            "--timestamp",
            help="Specifies a timestamp of the backup to be deleted",
        )

    @staticmethod
    def remove(args, collection: BackupCollection):
        if args.timestamp:
            timestamp = datetime.datetime.fromtimestamp(int(args.timestamp))
            try:
                collection.remove_backup(timestamp)
            except UnknownBackupError:
                raise ActionFailedError(
                    (
                        f"Unable remove backup with timestamp {timestamp}, "
                        "backup doesn't exist."
                    )
                )
        else:
            collection.remove_backup()

    @staticmethod
    def func(args):
        hourly, daily, custom = BackupCollection.from_backup_conf()
        collection_types = {
            "hourly": hourly,
            "daily": daily,
            "custom": custom,
        }

        collection = collection_types.get(args.type)
        ActionRemove.remove(args, collection)

        data = []
        for collection_type in collection_types.values():
            data += collection_type.data
        BackupCollection.dump_backup_conf(data=data, config_path=collection.config_path)
