from torthmcu.namespace import Namespace as BaseNamespace

from torthmcu.backup.action_list import ActionList
from torthmcu.backup.action_load import ActionLoad
from torthmcu.backup.action_perform import ActionPerform
from torthmcu.backup.action_remove import ActionRemove


class Namespace(BaseNamespace):
    name = "backup"
    description = "Handles backup actions of a Minecraft Server world."

    def __init__(self):
        super().__init__()
        self.add_action(ActionList())
        self.add_action(ActionLoad())
        self.add_action(ActionPerform())
        self.add_action(ActionRemove())
