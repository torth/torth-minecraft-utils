from torthmcu.action import Action as BaseAction

from torthmcu.backup.backup import BackupType, BACKUP_TYPES
from torthmcu.backup.backup_collection import BackupCollection


class ActionList(BaseAction):
    name = "list"
    description = "List all backups"

    def __init__(self):
        super().__init__()
        self.add_argument(
            "type",
            choices=BACKUP_TYPES,
            help="Specifies a type of the backup",
            nargs="?",
        )
        self.add_argument(
            "--raw",
            action="store_true",
            help="Print in raw, json format",
        )

    @staticmethod
    def func(args):
        hourly, daily, custom = BackupCollection.from_backup_conf()
        collection_types = {
            BackupType.HOURLY.value: hourly,
            BackupType.DAILY.value: daily,
            BackupType.CUSTOM.value: custom,
        }
    
        if args.type:
            collection_types = {
                key: value
                for key, value in collection_types.items()
                if key == args.type
            }
        if args.raw:
            print({key: value.data for key, value in collection_types.items()})
        else:
            for collection in collection_types.values():
                print(collection)
