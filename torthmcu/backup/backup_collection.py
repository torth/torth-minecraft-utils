import json
import datetime

from abc import ABC
from abc import abstractmethod
from pathlib import Path
from typing import Tuple, List
from dataclasses import dataclass, field

import torthmcu.constants as C
from torthmcu.config import AppConfig
from torthmcu.config import JsonConfig

from torthmcu.backup.backup import Backup, BackupType, BACKUP_TYPES


class MaxBackupCountReachedError(Exception):
    pass


class UnknownBackupError(Exception):
    pass


@dataclass
class BackupCollection(ABC):
    collection: List[Backup] = field(default_factory=list)
    config_path: Path = field(init=False)
    current: int = field(init=False)

    def __post_init__(self):
        self.config_path = Path.joinpath(
            Path(AppConfig().get("MINECRAFT_BACKUP_FOLDER")),
            C.BACKUP_CONF,
        )

    def __len__(self) -> int:
        return len(self.collection)

    def __eq__(self, other) -> bool:
        return self.collection == other.collection

    def __iter__(self):
        self.current = 0
        return self

    def __next__(self) -> Backup:
        if self.current >= len(self.collection):
            raise StopIteration
        self.current += 1
        return self.collection[self.current - 1]

    def __repr__(self) -> str:
        backup_list = [str(backup) for backup in self.collection]
        return "\n".join(backup_list)

    @property
    def data(self) -> list:
        return [b.data for b in self.collection]

    @classmethod
    def from_data(cls, data: list):
        return cls([Backup.from_data(b) for b in data])

    @staticmethod
    def from_backup_conf(types: Tuple[str, ...] = None) -> Tuple[Backup, ...]:
        types = types or BACKUP_TYPES
        config_path = Path.joinpath(
            Path(AppConfig().get("MINECRAFT_BACKUP_FOLDER", "")),
            C.BACKUP_CONF,
        )
        config = JsonConfig(config_path)
        backups = config.get("backups", [])
        result = []
        if BackupType.HOURLY.value in types:
            result.append(
                HourlyBackupCollection.from_data(
                    [b for b in backups if b.get("type") == HourlyBackupCollection.type]
                )
            )
        if BackupType.DAILY.value in types:
            result.append(
                DailyBackupCollection.from_data(
                    [b for b in backups if b.get("type") == DailyBackupCollection.type]
                )
            )
        if BackupType.CUSTOM.value in types:
            result.append(
                CustomBackupCollection.from_data(
                    [b for b in backups if b.get("type") == CustomBackupCollection.type]
                )
            )
        return tuple(result)

    @property
    @abstractmethod
    def type(self) -> str:
        pass

    @property
    @abstractmethod
    def max_count(self) -> int:
        # Maximum number of backups of this type.
        pass

    def sort(self):
        self.collection.sort(key=lambda x: x.date, reverse=True)

    def select_backup(self, date: datetime.datetime) -> Backup:
        backup = None
        for b in self.collection:
            if b.date == date:
                backup = b
                break
        if not backup:
            raise UnknownBackupError(f"Unable to find backup with timestamp: {date}.")
        return backup

    def create_backup(self, note: str = None) -> None:
        backup = Backup(type=self.type, note=note)
        backup.perform()
        self.add_backup(backup)

    def add_backup(self, backup: Backup) -> None:
        self.collection.append(backup)
        if self.max_count:
            while len(self) > self.max_count:
                self.remove_backup()

    def remove_backup(self, date: datetime.datetime = None) -> None:
        if date:
            backup = self.select_backup(date)
            self.collection.remove(backup)
        else:
            self.sort()
            backup = self.collection.pop()
        backup.remove()

    def load_backup(self, date: datetime.datetime = None) -> None:
        if date:
            backup = self.select_backup(date)
        else:
            self.sort()
            backup = self.collection[0]
        backup.load()

    @staticmethod
    def dump_backup_conf(data: list, config_path: Path) -> None:
        with open(config_path, "w") as file:
            json.dump({"backups": data}, file, indent=4)


class HourlyBackupCollection(BackupCollection):
    type = BackupType.HOURLY.value

    @property
    def max_count(self) -> int:
        return int(AppConfig().get("HOURLY_MAX_COUNT", 0))


class DailyBackupCollection(BackupCollection):
    type = BackupType.DAILY.value

    @property
    def max_count(self) -> int:
        return int(AppConfig().get("DAILY_MAX_COUNT", 0))


class CustomBackupCollection(BackupCollection):
    type = "custom"

    @property
    def max_count(self) -> int:
        return int(AppConfig().get("CUSTOM_MAX_COUNT", 0))

    def create_backup(self, note: str = None):
        if len(self.collection) == self.max_count:
            raise MaxBackupCountReachedError
        return super().create_backup(note)
