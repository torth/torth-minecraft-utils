import datetime

from torthmcu.action import Action as BaseAction
from torthmcu.action import ActionFailedError
from torthmcu.minecraft import MinecraftService
from torthmcu.minecraft import MinecraftServiceError

from torthmcu.backup.backup import BACKUP_TYPES
from torthmcu.backup.backup_collection import BackupCollection
from .backup_collection import UnknownBackupError


class ActionLoad(BaseAction):
    name = "load"
    description = "Load a backup"

    def __init__(self):
        super().__init__()
        self.add_argument(
            "type",
            choices=BACKUP_TYPES,
            help="Specifies a type of the backup",
        )
        self.add_argument(
            "--timestamp",
            help="Specifies a timestamp of the backup to be loaded",
        )

    @staticmethod
    def load(timestamp_str: str, collection: BackupCollection) -> None:
        timestamp = None
        if timestamp_str:
            timestamp = datetime.datetime.fromtimestamp(int(timestamp_str))
        service = MinecraftService()
        try:
            service.stop()
            collection.load_backup(timestamp)
            service.start()
        except (UnknownBackupError, MinecraftServiceError) as error:
            raise ActionFailedError(str(error))

    @staticmethod
    def func(args):
        hourly, daily, custom = BackupCollection.from_backup_conf()
        collection_types = {
            "hourly": hourly,
            "daily": daily,
            "custom": custom,
        }

        collection = collection_types.get(args.type)
        ActionLoad.load(args.timestamp, collection)

        data = []
        for collection_type in collection_types.values():
            data += collection_type.data
        BackupCollection.dump_backup_conf(data=data, config_path=collection.config_path)
