from torthmcu.action import Action as BaseAction
from torthmcu.action import ActionFailedError

from torthmcu.backup.backup import BackupType, BACKUP_TYPES
from torthmcu.backup.backup_collection import BackupCollection
from torthmcu.backup.backup_collection import MaxBackupCountReachedError


class ActionPerform(BaseAction):
    name = "perform"
    description = "Perform a backup"

    def __init__(self):
        super().__init__()
        self.add_argument(
            "type",
            choices=BACKUP_TYPES,
            help="Specifies a type of the backup",
        )
        self.add_argument(
            "--note",
            type=str,
            default="",
            action="store",
            help="Note or a comment added to the backup",
        )

    @staticmethod
    def func(args):
        hourly, daily, custom = BackupCollection.from_backup_conf()
        collection_types = {
            BackupType.HOURLY.value: hourly,
            BackupType.DAILY.value: daily,
            BackupType.CUSTOM.value: custom,
        }

        collection: BackupCollection = collection_types.get(args.type)
        try:
            collection.create_backup(args.note)
        except MaxBackupCountReachedError:
            raise ActionFailedError(
                (
                    "Unable to perform backup, "
                    f"maximum number of {collection.type} backups has been reached."
                )
            )

        data = []
        for collection_type in collection_types.values():
            data += collection_type.data
        BackupCollection.dump_backup_conf(data=data, config_path=collection.config_path)
