import json
import logging
from pathlib import Path
from configparser import ConfigParser

import torthmcu.constants as Constants


class JsonConfig(dict):
    def __init__(self, path: Path):
        config = {}
        logger = logging.getLogger(Constants.APP_NAME)
        try:
            with open(path) as file:
                config = json.load(file)
        except (FileNotFoundError, PermissionError) as error:
            logger.warning("Unable to read json file: %s, error: %s", path, error)
        except Exception as error:
            logger.warning("Unable to parse json file: %s, error: %s", path, error)
        super().__init__(config)


class AppConfig(JsonConfig):
    def __init__(self):
        super().__init__(Constants.APP_CONF_PATH)


class MinecraftConfig(dict):
    def __init__(self):
        config = ConfigParser()
        path = Path.joinpath(
            Path(AppConfig().get("MINECRAFT_SERVER_FOLDER")), "server.properties"
        )
        with open(path) as file:
            config.read_string("[top]/n" + file.read())
        super().__init__(config._sections.get("top"))
