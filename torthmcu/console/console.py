import os
from pathlib import Path

from torthmcu.config import AppConfig, MinecraftConfig


class Console:
    @property
    def mcrcon_path(self) -> None:
        tools_dir = Path(AppConfig().get("MINECRAFT_TOOLS_FOLDER"))
        return Path.joinpath(tools_dir, "mcrcon/mcrcon")

    def run(self, args: list) -> None:
        os.system(f"{self.mcrcon_path} {' '.join(args)}")

    def open(self):
        config = MinecraftConfig()
        args = [
            "-P",
            config.get("rcon.port"),
            "-p",
            config.get("rcon.password"),
        ]
        self.run(args)
