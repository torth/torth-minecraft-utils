from torthmcu.namespace import Namespace as BaseNamespace

from torthmcu.console.action_open import ActionOpen


class Namespace(BaseNamespace):
    name = "console"
    description = "Handles console actions."

    def __init__(self):
        super().__init__()
        self.add_action(ActionOpen())
