from torthmcu.action import Action as BaseAction
from torthmcu.console.console import Console


class ActionOpen(BaseAction):
    name = "open"
    description = "Opens the console"

    def __init__(self):
        super().__init__()

    @staticmethod
    def func(_):
        console = Console()
        console.open()
