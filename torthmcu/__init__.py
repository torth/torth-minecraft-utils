import os
import sys

from grp import getgrnam
from pwd import getpwnam

import torthmcu.constants as C
from torthmcu.app import App
from torthmcu.backup.namespace import Namespace as BackupNamespace
from torthmcu.console.namespace import Namespace as ConsoleNamespace
from torthmcu.service.namespace import Namespace as ServiceNamespace


def switch_user(user: str):
    try:
        user_id = getpwnam(C.RUN_AS_USER).pw_uid
    except KeyError:
        print(f"Unable to find user {user}. Exiting.")
        sys.exit(1)
    try:
        os.setuid(user_id)
    except PermissionError:
        print(f"Unable to switch to user {user}, insufficient permissions. Exiting.")
        sys.exit(1)


def switch_group(group: str):
    try:
        group_id = getgrnam(C.RUN_AS_GRP).gr_gid
    except KeyError:
        print(f"Unable to find group {group}. Exiting.")
        sys.exit(1)
    try:
        os.setgid(group_id)
    except PermissionError:
        print(f"Unable to switch to group {group}, insufficient permissions. Exiting.")
        sys.exit(1)


def main():
    switch_group(C.RUN_AS_GRP)
    switch_user(C.RUN_AS_USER)
    app = App()
    app.add_namespace(BackupNamespace())
    app.add_namespace(ConsoleNamespace())
    app.add_namespace(ServiceNamespace())
    app.run()
    sys.exit(app.return_code)
