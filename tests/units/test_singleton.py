from torthmcu.singleton import Singleton


def test_singleton():
    class MockSingleton(metaclass=Singleton):
        pass

    class MockSingletonOther(metaclass=Singleton):
        pass
    assert id(MockSingleton()) == id(MockSingleton())
    assert id(MockSingleton()) != id(MockSingletonOther())
