import pytest
import datetime

from torthmcu.backup.backup import Backup
from torthmcu.backup.backup_collection import UnknownBackupError
from torthmcu.backup.backup_collection import MaxBackupCountReachedError
from torthmcu.backup.backup_collection import BackupCollection


@pytest.fixture(autouse=True)
def mock_json_config(monkeypatch):
    class JsonConfig(dict):
        def __init__(self, *args, **kwargs):
            super().__init__({
                'backups': [
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-21-15-25.tar.gz',
                        'type': 'hourly',
                        'date': '1632230739',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-21-14-25.tar.gz',
                        'type': 'hourly',
                        'date': '1632227139',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-21-13-25.tar.gz',
                        'type': 'hourly',
                        'date': '1632223539',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-21-12-25.tar.gz',
                        'type': 'hourly',
                        'date': '1632219939',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-21-15-25.tar.gz',
                        'type': 'daily',
                        'date': '1632230739',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-20-15-25.tar.gz',
                        'type': 'daily',
                        'date': '1632144339',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-19-15-25.tar.gz',
                        'type': 'daily',
                        'date': '1632057939',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-18-15-25.tar.gz',
                        'type': 'daily',
                        'date': '1631971539',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-21-15-25.tar.gz',
                        'type': 'custom',
                        'date': '1632230739',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-20-15-25.tar.gz',
                        'type': 'custom',
                        'date': '1632144339',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-19-15-25.tar.gz',
                        'type': 'custom',
                        'date': '1632057939',
                    },
                    {
                        'path': '/opt/minecraft/backup/server-2021-09-18-15-25.tar.gz',
                        'type': 'custom',
                        'date': '1631971539',
                    },
                ]
            })
    monkeypatch.setattr('torthmcu.backup.backup_collection.JsonConfig', JsonConfig)


@pytest.fixture(autouse=True)
def disable_backup_actions(monkeypatch):
    class MockBackup(Backup):
        def __init__(self, *args, **kwargs):
            self.test_performed = False
            self.test_removed = False
            self.test_loaded = False
            super().__init__(*args, **kwargs)

        def perform(self):
            self.test_performed = True

        def remove(self):
            self.test_removed = True

        def load(self):
            self.test_loaded = True
    monkeypatch.setattr('torthmcu.backup.backup_collection.Backup', MockBackup)


def test_from_backup_conf():
    hourly, daily, custom = BackupCollection.from_backup_conf()
    assert len(hourly) == 4
    assert len(daily) == 4
    assert len(custom) == 4


def test_select_backup():
    custom = BackupCollection.from_backup_conf()[2]
    date = datetime.datetime.fromtimestamp(1631971539)
    backup = custom.select_backup(date)
    assert backup.date == date
    assert backup in custom.collection


def test_select_backup_failed():
    custom = BackupCollection.from_backup_conf()[2]
    date = datetime.datetime.fromtimestamp(0)
    with pytest.raises(UnknownBackupError):
        custom.select_backup(date)


def test_remove_oldest_backup():
    custom = BackupCollection.from_backup_conf()[2]
    date = datetime.datetime.fromtimestamp(1631971539)
    backup = custom.select_backup(date)
    assert backup in custom.collection
    custom.remove_backup()
    assert date not in [b.date for b in custom.collection]
    assert backup.test_removed
    assert [b.test_removed for b in custom.collection if b.test_removed] == []


def test_create_backup():
    custom = BackupCollection.from_backup_conf()[2]
    custom.remove_backup()
    number_of_backups = len(custom)
    custom.create_backup()
    assert len(custom) == number_of_backups + 1
    assert custom.collection[-1].test_performed


def test_create_backup_max_count_reached():
    custom = BackupCollection.from_backup_conf()[2]
    with pytest.raises(MaxBackupCountReachedError):
        custom.create_backup()


def test_create_backup_auto_remove():
    hourly = BackupCollection.from_backup_conf()[0]
    date = datetime.datetime.fromtimestamp(1632219939)
    backup = hourly.select_backup(date)
    number_of_backups = len(hourly)
    assert backup in hourly.collection
    hourly.create_backup()
    assert backup not in hourly.collection
    assert number_of_backups == len(hourly)


def test_remove_backup():
    custom = BackupCollection.from_backup_conf()[2]
    date = datetime.datetime.fromtimestamp(1632144339)
    backup = custom.select_backup(date)
    assert backup in custom.collection
    custom.remove_backup(date)
    assert date not in [b.date for b in custom.collection]
    assert backup.test_removed


def test_load_backup():
    custom = BackupCollection.from_backup_conf()[2]
    date = datetime.datetime.fromtimestamp(1631971539)
    backup = custom.select_backup(date)
    custom.load_backup(date)
    assert backup in custom.collection
    assert backup.test_loaded


def test_load_newest_backup():
    custom = BackupCollection.from_backup_conf()[2]
    date = datetime.datetime.fromtimestamp(1632230739)
    backup = custom.select_backup(date)
    assert backup in custom.collection
    custom.load_backup()
    assert backup.test_loaded
    assert [b.test_loaded for b in custom.collection if b.test_loaded] == [True]
