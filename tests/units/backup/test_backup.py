import pytest
import datetime
from pathlib import Path

from torthmcu.backup.backup import Backup


def test_backup_init_only_type():
    backup_type = 'custom'
    backup = Backup(backup_type)
    assert backup.type == backup_type
    assert isinstance(backup.date, datetime.datetime)
    assert isinstance(backup.path, Path)


def test_backup_init_all_args():
    backup_type = 'custom'
    path = Path('/foo/bar')
    date = datetime.datetime.now()
    backup = Backup(backup_type, path, "", date)
    assert backup.type == backup_type
    assert backup.path == path
    assert backup.date == date
