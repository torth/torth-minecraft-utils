import pytest


@pytest.fixture(autouse=True)
def mock_config(monkeypatch):
    class AppConfig(dict):
        def __init__(self):
            super().__init__({
                "APP_LOG_LEVEL": 20,
                "MINECRAFT_SERVER_FOLDER": "/opt/minecraft/server",
                "MINECRAFT_BACKUP_FOLDER": "/opt/minecraft/backup",
                "HOURLY_MAX_COUNT": 4,
                "DAILY_MAX_COUNT": 4,
                "CUSTOM_MAX_COUNT": 4
            })
    monkeypatch.setattr('torthmcu.backup.backup.AppConfig', AppConfig)
    monkeypatch.setattr('torthmcu.backup.backup_collection.AppConfig', AppConfig)
