import os

from setuptools import setup, find_packages
from setuptools.command.bdist_rpm import bdist_rpm as BdistRpm

# Files/directories to be included in the package.
# Entry format: tuple(destination, source)
data_files = (
    ('/etc', 'install/etc'),
)

# File permission specification.
# Entry format: tuple(path, attributes)
permissions = (
    ('/etc/sysconfig/*', '(0644, root, root) %config(noreplace)'),
)


def get_file_content(filename: str):
    try:
        with open(filename, 'r') as f:
            return f.read()
    except (FileNotFoundError, PermissionError):
        return ''


def get_requirements(filename: str):
    return get_file_content(filename).splitlines()


def get_version():
    version = os.getenv('VERSION')
    if version:
        return version
    if os.path.exists('version'):
        return get_file_content('version')
    return '1.0.0'


def expand_data_files(paths: tuple):
    """Generate a list of associations between source file and target locations.
    Args:
        paths (tuple): tuple of tuples(destination (str), source (str))

    Returns:
        tuple: tuple of tuples, each representing expanded destination(str) and source(str)
    """
    result = []
    for dst, src in paths:
        for (path, _, filenames) in os.walk(src):
            if filenames:
                destination = path.replace(src, dst)
                sources = [os.path.join(path, filename) for filename in filenames]
                result.append((destination, sources))
    return result


def get_dynamic_setup_params():
    return {
        'version': get_version(),
        'long_description': get_file_content('README.md'),
        'install_requires': get_requirements('requirements.txt'),
        'data_files': expand_data_files(data_files)
    }


class BdistRpmCommand(BdistRpm):
    """Bdist_rpm class with extended functionality
    that allows us to define specific file permissions and more.
    """

    def _make_spec_file(self):
        spec = super()._make_spec_file()
        # Disable build of the debuginfo rpm package
        spec.insert(0, '%define debug_package %{nil}')
        # Specify file permissions
        for path, perm in permissions:
            spec.extend([f'%attr{perm} {path}'])
        return spec


static_setup_params = dict(
    name='torthmcu',
    author='Thomas Orth',
    author_email='orth.th@gmail.com',
    description='TOrth\'s Minecraft Utils',
    packages=find_packages('.'),
    entry_points={
        'console_scripts': [
            'torthmcu = torthmcu:main',
        ],
    },
    python_requires='>=3.9',
    cmdclass={
        'bdist_rpm': BdistRpmCommand,
    },
)


def main():
    setup_params = dict(static_setup_params, **get_dynamic_setup_params())
    setup(**setup_params)


if __name__ == '__main__':
    main()
